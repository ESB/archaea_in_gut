import pandas as pd
import numpy as np
import os
import time

start_time = time.time()

cluster_name = os.environ['cluster_name']
dir_target = os.environ['dir_target']
protein_type = os.environ['protein_type']
dir_main = os.environ['dir_main']

# upload target genomes from SC:
genomes_in_SC = pd.read_csv('/work/projects/archaea_neurodeg/coevolution/hgt_analysis/hgt_genes/'+protein_type+'/genomes_in_SCs/genomes_in_'+
                            cluster_name+'.tsv',
                           header=None, names=['genome'])

# PART 1: run the script to change the prodigal output:
print('--- updating prodgial output ---')
final = pd.DataFrame()

for i in range(0,len(genomes_in_SC)):
    genome = genomes_in_SC.genome.iloc[i]
    # print(genome)
    prodigal = pd.read_csv('/work/projects/archaea_neurodeg/coevolution/hgt_analysis/prodigal/'+genome+'.gff',
                           sep='\t', header=None, index_col=False,
                           names=['genome', 'prodigal', 'cds', 'start', 'end', 'smth', 'strand', 'smth1', 'etc'])
    prodigal = prodigal[['genome', 'start', 'end', 'strand', 'etc']]
    
    prodigal.start = prodigal.start + 1

    m = prodigal['strand'].eq('-')
    prodigal['coordinates'] = (np.where(m, 'complement(', '')
                        +prodigal['start'].astype(str)+'..'+prodigal['end'].astype(str)
                        +np.where(m, ')', '')
                        )

    prodigal['protein_number'] = prodigal['etc'].str.split(';').str[0].str.replace('ID=1', '')

    prodigal['protein'] = prodigal.genome.astype('str') + prodigal.protein_number.astype('str')

    prodigal = prodigal[['genome', 'coordinates', 'protein']]
    
    final = pd.concat([final, prodigal], axis=0)
    

updated_prodigal = final
updated_prodigal['SC'] = cluster_name
updated_prodigal.to_csv(dir_target + '/output_prodigal_updated/' + cluster_name+'.tsv', sep='\t', index=False)


# PART 2: merge metachip output

print('--- merging with metachip ---')
# read the original metachip output:
metachip = pd.read_csv(dir_main + '/hgt_genes/consistent_genomes_classification_coords.txt', sep='\t')

# this magic here is to account for genomes being either "donors" or "acceptors" of HGT:
Gene1_upd_prodigal = updated_prodigal.rename(columns={'genome':'Gene1_contig',
                                                      'coordinates':'Gene1_coordinate'})
Gene2_upd_prodigal = updated_prodigal.rename(columns={'genome':'Gene2_contig',
                                                      'coordinates':'Gene2_coordinate'})

merge_Gene1 = metachip.merge(Gene1_upd_prodigal, on=['Gene1_contig', 'Gene1_coordinate'])
merge_Gene2 = metachip.merge(Gene2_upd_prodigal, on=['Gene2_contig', 'Gene2_coordinate'])

# the "decoded" metachip output:
pd.concat([merge_Gene1, merge_Gene2], axis=0).to_csv('/work/projects/archaea_neurodeg/coevolution/hgt_analysis/hgt_genes/' + protein_type + '/decoded_metachip/' + cluster_name + '.tsv', sep='\t', index=False)


print("--- task took %s seconds ---" % (round(time.time(), 2) - round(start_time, 2)))