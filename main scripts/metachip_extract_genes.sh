# check what proteins are in the cluster: 
cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/uniq/lists/unc_1.tsv | while read protein; do
    grep $protein /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes_archaea.tsv | cut -f2 | sed 's/.fasta//g' > tmp.tsv 
    cat tmp.tsv
    # cat tmp.tsv | while read genome; do
    #     grep $genome /work/projects/archaea_neurodeg/coevolution/hgt_analysis/hgt_genes/consistent_genomes_classification_coords.txt
    # done
done 

protein=GUT_GENOME270877.fasta_297 # as i have it
new_protein=GUT_GENOME270877_01659 # as metachip uses 
genome=GUT_GENOME270877.fasta

GUT_GENOME270877_01659  GUT_GENOME264666_00410  100.0   no      no      GUT_GENOME264666-->GUT_GENOME270877     GUT_GENOME270877.fasta  GUT_GENOME264666.fasta  complement(1652098..1652402)    374295..374599


# -------------------------------------------------------------------------------------------------------- 
# run prodigal to fetch coordinates
conda activate prodigal

# cd /work/projects/archaea_neurodeg/coevolution/hgt_analysis/consistent_genomes
# for genome in *.fasta; do
#     prodigal -i /work/projects/archaea_neurodeg/coevolution/hgt_analysis/consistent_genomes/$genome -f gbk -o /work/projects/archaea_neurodeg/coevolution/hgt_analysis/prodigal/$(basename "${genome}" .fasta).gbk
# done

cd /work/projects/archaea_neurodeg/coevolution/hgt_analysis/consistent_genomes
for genome in *.fasta; do
    prodigal -i /work/projects/archaea_neurodeg/coevolution/hgt_analysis/consistent_genomes/$genome -f gff -o /work/projects/archaea_neurodeg/coevolution/hgt_analysis/prodigal/$(basename "${genome}" .fasta).gff
done

# remove 3 first lines
cd /work/projects/archaea_neurodeg/coevolution/hgt_analysis/prodigal/
sed -i '/^#/d' *gff 
# -------------------------------------------------------------------------------------------------------- 

metachip=/work/projects/archaea_neurodeg/coevolution/hgt_analysis/hgt_genes/consistent_genomes_classification_coords.txt
/work/projects/archaea_neurodeg/coevolution/hgt_analysis/hgt_genes/uniq/test/GUT_GENOME270877.gff

# the idea is to take the gff file, add +1 to the start coordinate to match to metachip format, contact it to  

# -------------------------------------------------------------------------------------------------------- 
# --------------------------------------------------------------------------------------------------------


dir_main=/work/projects/archaea_neurodeg/coevolution/hgt_analysis
protein_type=homo
dir_SCs=/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/$protein_type/lists
mkdir -p $dir_main/hgt_genes/$protein_type
dir_target=$dir_main/hgt_genes/$protein_type
export dir_main
export protein_type
export dir_SCs
export dir_target


rm $dir_target/genomes_in_SCs/*.tsv
mkdir -p $dir_target/genomes_in_SCs
mkdir -p $dir_target/output_prodigal_updated
mkdir -p $dir_target/decoded_metachip

for file in $dir_SCs/*.tsv; do
    cluster_name=$(basename "$file" .tsv)
    echo "-------------------"
    echo "SC $cluster_name running"
    echo "-------------------"
    export cluster_name
    conda activate prodigal
    cat $file | while read protein; do
        grep $protein /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes_archaea.tsv | cut -f2 | sed 's/.fasta//g' | uniq | while read genome; do
            echo "prodigal for $genome"
            prodigal -i /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/$genome.fasta -f gff -o $dir_main/prodigal/$genome.gff -q
        done
        # remove 3 first lines
        sed -i '/^#/d' $dir_main/prodigal/*gff
        grep $protein /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes_archaea.tsv | cut -f2 | sed 's/.fasta//g' | uniq >> $dir_target/genomes_in_SCs/genomes_in_$cluster_name.tsv
    done
    conda activate jupyter
    python $dir_main/link_metachip_to_prodigal.py
    echo "-----------------"
    echo "SC $cluster_name done"
    echo "-----------------"
done
