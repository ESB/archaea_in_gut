library(ggtree)
library(dplyr)
library(stringr)

tree <- read.tree("h20+blast_msa_trimmed.fasta.treefile")
p <- ggtree(tree, branch.length="none")
# p
# msaplot(p=ggtree(tree), fasta="h20+blast_msa.fasta") + theme(legend.position="none")


# Adding colors to tips
colours <- data.frame(label=tree$tip.label) %>%
  mutate(taxa=case_when(
    str_detect(label, ".fasta_") ~ "GUT_archaea",
    str_detect(label, "tr_") ~ "uniprot",
    TRUE ~ "GUT_bacteria"),
    color=case_when(
      str_detect(label, ".fasta_") ~ "pink",
      str_detect(label, "tr_") ~ "#bababa",
      TRUE ~ "blue"))
merged_tree <- full_join(tree, colours, by='label') %>% na.omit()
merged_tree@data$color[is.na(merged_tree@data$color)] <- "missing"
merged_tree@data$taxa[is.na(merged_tree@data$taxa)] <- "missing"

merged_plot <- ggtree(merged_tree, aes(color=taxa), size=0.3) +
  scale_x_continuous() +
  geom_tippoint(aes(color=taxa), size=0.5) +
  geom_tiplab(aes(color=taxa), hjust = -.1, size=0.8, color="black") +
  scale_color_manual(values=c(GUT_archaea = "deeppink3", uniprot = "grey81",
                              GUT_bacteria = "blue4")) +
  theme(legend.position='right', legend.key.size = unit(0.1, 'cm'))
msaplot(p=merged_plot, fasta="h20+blast_msa_trimmed.fasta")



