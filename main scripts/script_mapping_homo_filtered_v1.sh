#!/bin/bash -l
#SBATCH -J mapping_h_f
#SBATCH -o mapping_h_filtered_%j.log
#SBATCH -N 2
#SBATCH -n 128
#SBATCH --time=14-00:00:00
#SBATCH --partition=batch
#SBATCH --qos=long
#SBATCH --begin=now
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

conda activate mapping

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

# index the reference
bwa index $dir/proteins_homo_arc_bac_n.fa

cat $dir/must_family_ids.tsv | while read family; do
	echo $family
	cat $dir/${family}.tsv | while read sample; do
		echo $sample

		reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
		reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

		mkdir -p $dir/mapping/homo_arc_bac_filtered/

		# paired-end mapping, general command structure, adjust to your case
		bwa mem $dir/proteins_homo_arc_bac_n.fa $reads1 $reads2 > $dir/mapping/homo_arc_bac_filtered/${sample}.aln_pe.sam

		# fix mates and compress
		samtools sort -n -O sam $dir/mapping/homo_arc_bac_filtered/${sample}.aln_pe.sam | samtools fixmate -m -O bam - $dir/mapping/homo_arc_bac_filtered/${sample}.fixmate.bam

		# convert to bam file and sort
		samtools sort -O bam -o $dir/mapping/homo_arc_bac_filtered/${sample}.sorted.bam $dir/mapping/homo_arc_bac_filtered/${sample}.fixmate.bam

		# Once it successfully finished, delete the fixmate file and the sam file to save space
		rm $dir/mapping/homo_arc_bac_filtered/${sample}.fixmate.bam
		rm $dir/mapping/homo_arc_bac_filtered/${sample}.aln_pe.sam

		# get median read coverage for each gene:
		samtools index $dir/mapping/homo_arc_bac_filtered/${sample}.sorted.bam
		cd $dir/mapping/homo_arc_bac_filtered/
		mosdepth ${sample} $dir/mapping/homo_arc_bac_filtered/${sample}.sorted.bam --use-median -t 256
	done
done
