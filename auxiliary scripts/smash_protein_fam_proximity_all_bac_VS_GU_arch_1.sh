#!/bin/bash -l                                                                                                        
#SBATCH -J smash                
#SBATCH -o smash_1.log          
#SBATCH -N 8                    
#SBATCH -c 8                   
#SBATCH --time=48:00:00         
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -p batch 

date

conda activate smash

main_dir=/work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/

mkdir -p $main_dir/sig
mkdir -p $main_dir/out

# create signatures
cd /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs

for f in *.fasta; do echo $f ; sourmash compute --protein --input-is-protein -k 51 $f -o $main_dir/sig/$f.sig; done

echo "create sigs complete"
date

# index

sourmash index -k 31 $main_dir/sbt_08 $main_dir/sig/*.sig
# compare

sourmash compare -p 16 $main_dir/sig/*.sig -o $main_dir/out/08_cmp --csv $main_dir/out.csv

cd $main_dir
rm -r sig/            
rm -r out/

date
