import requests
import re


def converter(gtdb_id):
    url = 'https://gtdb.ecogenomic.org/genomes?gid=' + gtdb_id
    webpage = None
    c = 0
    while not webpage and c <= 10:
        req = requests.get(url)
        try:
            webpage = req.text
        except:
            print('Failed, trying again')
            c += 1
        c += 1
        taxa_pos_start = re.search('<th>Species Taxid</th>', webpage)
        if taxa_pos_start:
            taxa_pos_start = taxa_pos_start.span()[1]
            webpage = webpage[taxa_pos_start:]
            taxa_id = re.search('<td>\d+</td>', webpage)
            if taxa_id:
                taxa_id = taxa_id.group().strip('<td>/')
                return taxa_id


def read_gtdb_files(file_path, outfile_path):
    temp_conversion = {}
    with open(outfile_path, 'w+') as outfile:
        with open(file_path) as file:
            line = file.readline()
            while line:
                gtdb_id, org_name = None, None
                line = line.strip('\n').split('\t')
                line = [i for i in line if i]
                if len(line) == 8 and 'UBA' not in line[-1]: gtdb_id = line[-1]
                if not gtdb_id:
                    while line:
                        if not re.search('[A-Z][a-z]+', line[-1]) or '-' in line[-1]:
                            line.pop()
                        else:
                            break
                if gtdb_id:
                    if gtdb_id not in temp_conversion:
                        line_res = converter(gtdb_id)
                        temp_conversion[gtdb_id] = line_res
                    else:
                        line_res = temp_conversion[gtdb_id]
                else:
                    line_res = line[-1].split('_')[0]
                if len(line) == 1: line_res = ''
                print(line[0], line_res)
                outfile.write(line[0] + '\t' + str(line_res) + '\n')
                line = file.readline()


gem_path = '/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/input/tmp_tax_gut'
outfile = '/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/input/tmp_id2ncbi_id_gut.tsv'
read_gtdb_files(gem_path, outfile)

