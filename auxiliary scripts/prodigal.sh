#!/bin/bash -l
#SBATCH -J prod_gut_bac
#SBATCH -o prod_gut_bac.log
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --time=48:00:00
#SBATCH -p bigmem

date 

conda activate prodigal

cd /work/projects/ecosystem_biology/archaea/coevolution/

cut -f1 analysis/intermediate_results/reduced_GUT_catalogue.csv | cat | while read genome; do
	prodigal -i /scratch/users/pnovikova/bacteria/data/genomes_gut/${genome}.fasta -a /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/${genome}.proteins -f gff;
done
