import requests
import re

def get_taxa_ncbi_url(url):
    webpage = None
    c = 0
    while not webpage and c <= 10:
        req = requests.get(url)
        try:
            webpage = req.text
            taxa_id = re.search('<Id>\d+</Id>', webpage)
            return re.search('\d+', taxa_id.group()).group()
        except:
            print('Could not get a response from NCBI, trying again')
            c += 1


def get_taxa_ncbi(organism_name):
    url = f'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=taxonomy&term={organism_name}'
    taxa_id = get_taxa_ncbi_url(url)
    if not taxa_id:
        url = f'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=taxonomy&term=candidatus+{organism_name}'
        taxa_id = get_taxa_ncbi_url(url)
    if not taxa_id:    print(f'Could not find taxa ID for {organism_name}')
    return taxa_id

def read_gtdb_files2(file_path, outfile_path):
    temp_conversion = {}
    with open(outfile_path, 'w+') as outfile:
        firstline = 'Genome_ID\tSearch_name\tNCBI_ID\n'
        outfile.write(firstline)

        with open(file_path) as file:
            line = file.readline()
            while line:
                line = line.strip('\n').split('\t')
                line = [i for i in line if i]
                genome_id,l_domain, l_phylum, l_class, l_order, l_family = line[0],line[1],line[2],line[3],line[4],line[5]
                l_genus,l_species='',''
                if len(line)==8:                    l_genus, l_species = line[-2],line[-1]
                if len(line)==7:                    l_genus = line[-1]
                if re.search('[A-Z]{2}', l_species) or re.search('\d', l_species): l_species=''
                if re.search('[A-Z]{2}', l_genus) or re.search('\d', l_genus): l_genus=''
                if l_species: search_name=l_species
                elif l_genus and not l_species: search_name=l_genus
                else: search_name=l_family
                search_name=' '.join([i.split('_')[0] for i in search_name.split()])
                ncbi_id=get_taxa_ncbi(search_name)
                line=f'{genome_id}\t{search_name}\t{ncbi_id}\n'
                print(line)
                outfile.write(line)
                line = file.readline()

gem_path = '/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/input/tmp_tax_gut'
outfile = '/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/input/tmp_id2ncbi_id_gut.tsv'
read_gtdb_files2(gem_path, outfile)
