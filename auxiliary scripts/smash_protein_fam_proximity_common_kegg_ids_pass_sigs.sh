#!/bin/bash -l
#SBATCH -J smash
#SBATCH --time=14-00:00:00
#SBATCH -p bigmem
#SBATCH --qos=long
#SBATCH -o smash_common_kegg_ids_pass_sigs_%j.log
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -N 1
#SBATCH -n 88
#SBATCH --mem=100TB


conda activate smash

main_dir=/work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch

mkdir -p $main_dir/out

cd $main_dir/sig

# index                  
sourmash index -k 51 -f $main_dir/sbt_08 --from-file $main_dir/list_of_sigs_1.txt

# compare                                                
sourmash compare -p 16 --from-file $main_dir/list_of_sigs_1.txt -o $main_dir/out/08_cmp --csv $main_dir/out.csv
