#!/bin/bash -l
#SBATCH -J tcoffee
#SBATCH -o tcoffee.log
#SBATCH -N 4
#SBATCH -c 8
#SBATCH --time=48:00:00
#SBATCH -p batch

conda activate tcoffee

main_dir=/work/projects/coevolution/phylogeny/tcoffee/FR_bac_VS_FR_arch
pfs=/work/projects/coevolution/phylogeny/smash/FR_bac_VS_FR_arch/PFs_2plus_proteins

cd $main_dir

date

for file in $pfs/*.fasta; do
	t_coffee $file
	rm $(basename "${file}" .fasta).html
done

date
