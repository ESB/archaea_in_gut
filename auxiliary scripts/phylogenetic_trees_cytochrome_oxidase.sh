#!/bin/bash -l
#SBATCH -J ph_oxidase
#SBATCH -o phylogenetic_trees_cytochrome_oxidase_%j.log
#SBATCH -N 8
#SBATCH -c 16
#SBATCH --time=48:00:00
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -p batch
#SBATCH --mem=100GB

cd /work/projects/coevolution/phylogenetic_trees/cyctochrome_oxidase_family

conda activate tcoffee

t_coffee /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/cyctochrome_oxidase_family.fasta

